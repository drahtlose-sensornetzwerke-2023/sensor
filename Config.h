#define SSID "SensorTest"
#define PASSWORD "sensor_is_alive"
#define LOG_INTERVAL 1
#define DHT_PIN 33
#define INFLUXDB_PORT "8086"
#define DB_NAME "mydb"
#define SD_SELECT_PIN 32
#define DISABLE_REALTIME_LOGGING false
// Is useful when you want to manually check the logging-to-SD-card functionality
#define DISABLE_OLD_LOG_REMOVAL true
#define LOG_PATH "/log.bin"