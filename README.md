3rd party libraries used:
  - RTClib by Adafruit
  - Sensirion I2C SGP41 by Sensirion
  - Sensirion I2C SHT4x by Sensirion

Rest of the libraries (HTTPClient, protobuf-c) come from the ESP32 platform, with the exception of generated code "measurement.pb-c". If you modify the measurement structure (add new fields, remove fields, etc.), you need to regenerate the code as instructed in [protobuf-c documentation](https://github.com/protobuf-c/protobuf-c#synopsis).

## Debugging
You may notice that the code does not have a lot of serial prints to report errors. This is because we rely on the debug output from the libraries instead. You can activate them from "Tools > Core Debug Level Level" in the Arduino IDE menu bar.

## SD Card Issues
The board cannot be flashed with the SD card plugged in. After bootup, the user has 10 seconds to plug in the SD card. 

During bootup the code will wait for SD card to work but if the connection gets severed after the initialization, the code will run as normal and you will only be notified from the debug output if it is set to the appropriate level (set to verbose to make sure). When the connection is broken while the program is running, the filesystem can get corrupted and cause the writes to fail even if the connection is fixed later. In that case, reformat the drive.

The SD card module does not reliably work when the board is powered only through the microcontroller's USB port (without the PCB's). You may be able to get it to work by pressing on the module but it is not reliable and you need to check the serial output for errors. 

To make use of the SD card module, connect USB power to the sensor PCB and switch the red jumper near the connector from SW to PWR. You can safely flash firmware or keep the connection between the computer and the microcontroller in general (for example, to read the serial output) when the powerbank is plugged in.

## RTC Setup
`SensorLogger` has no logic to update the RTC clock in the case it gets reset or is wrong for any reason. `UpdateRTCTime` needs to be run in that case to update the time.

## Database Connection
An InfluxDB instance needs to be set up on the access point and a database created. The values in the `Config.h` should me modified accordingly.
