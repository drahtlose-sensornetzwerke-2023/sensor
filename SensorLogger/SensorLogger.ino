#include <HTTPClient.h>
#include <protobuf-c/protobuf-c.h>
#include <RTClib.h>
#include <SD.h>
#include <SensirionI2CSgp41.h>
#include <SensirionI2CSht4x.h>
#include <SPI.h>
#include <Ticker.h>
#include <WiFi.h>
#include <Wire.h>
#include "Config.h"
#include "measurement.pb-c.h"

#define DEFAULT_RH 0x8000
#define DEFAULT_T 0x6666

RTC_DS3231 rtc;
SensirionI2CSgp41 sgp41;
SensirionI2CSht4x sht4x;
uint16_t conditioning_s = 10;

SPIClass sd(HSPI);

Ticker sensorLogger;
Ticker sgp41Conditioner;

// For details see https://protobuf-c.github.io/protobuf-c/structProtobufCBuffer.html#details
typedef struct {
  ProtobufCBuffer base;
  File file;
} BufferAppendToFile;

void my_buffer_file_append(ProtobufCBuffer *buffer,
                           size_t len,
                           const uint8_t *data) {
  BufferAppendToFile *file_buf = (BufferAppendToFile *)buffer;
  file_buf->file.write(data, len);
}

void onGotIP(WiFiEvent_t event, WiFiEventInfo_t info) {
  // We suspend the periodic logging to preempt conflicts it may cause with the file upload
  sensorLogger.detach();

  HTTPClient http_client;

  http_client.begin(String("http://") + WiFi.gatewayIP().toString() + ":8000/");

  http_client.addHeader("Content-Type", "application/x-www-form-urlencoded");
  File file = SD.open(LOG_PATH, FILE_READ);
  http_client.sendRequest("POST", &file, file.size());
  http_client.end();
  if (!DISABLE_OLD_LOG_REMOVAL) {
    SD.remove(LOG_PATH);
  }

  sensorLogger.attach(LOG_INTERVAL, logSensors);
}

void onDisconnected(WiFiEvent_t event, WiFiEventInfo_t info) {
  // Ignore unsuccessful reconnect attempts, only set up reconnection for dropped connections
  if (info.disconnected.reason != WIFI_REASON_NO_AP_FOUND) {
    // We need to explicitly request it to try to reconnect after a connection falls
    WiFi.reconnect();
  }
}

void logSensors() {
  float humidity;
  float temperature;
  sht4x.measureHighPrecision(temperature, humidity);
  uint16_t srawVoc = 0;
  uint16_t srawNox = 0;
  sgp41.measureRawSignals(DEFAULT_RH, DEFAULT_T, srawVoc, srawNox);
  DateTime time = rtc.now();
  Serial.println(time.timestamp());
  uint32_t unixtime = time.unixtime();

  String payload = String("sensors humidity=") + humidity
                   + ",temperature=" + temperature
                   + ",raw_voc=" + srawVoc
                   // the "i" after the value means that the value is of type integer
                   + "i,raw_nox=" + srawNox
                   // InfluxDB uses 9 extra digits of precision
                   + "i " + unixtime + "000000000";

  Serial.println(payload);

  int http_code = -1;
  if (!DISABLE_REALTIME_LOGGING && WiFi.isConnected()) {
    HTTPClient http_client;
    http_client.begin(String("http://") + WiFi.gatewayIP().toString() + ":" + INFLUXDB_PORT "/write?db=" + DB_NAME);
    http_client.addHeader("Content-Type", "application/x-www-form-urlencoded");
    http_code = http_client.POST(payload);
    http_client.end();
  }
  // The previous conditional did not run or the request did not succeed
  if (http_code < 0) {
    Measurement measurement = MEASUREMENT__INIT;
    measurement.humidity = humidity;
    measurement.temperature = temperature;
    measurement.raw_voc = srawVoc;
    measurement.raw_nox = srawNox;
    measurement.unixtime = unixtime;

    File file = SD.open(LOG_PATH, FILE_APPEND);
    BufferAppendToFile tmp = { 0 };
    tmp.base.append = my_buffer_file_append;
    tmp.file = file;

    // Even if the all messages have the same fields, it turns out they can vary in size.
    // For this reason, we have to declare the size of the message before it,
    // as suggested in https://protobuf.dev/programming-guides/techniques/#streaming
    // Our message size should not exceed UINT8_MAX (255)
    file.write((uint8_t) measurement__get_packed_size(&measurement));
    measurement__pack_to_buffer(&measurement, (ProtobufCBuffer *)&tmp);
    file.close();
  }
}

void conditionSgp41() {
  uint16_t srawVoc = 0;
  if (conditioning_s > 0) {
    // During NOx conditioning (10s) SRAW NOx will remain 0
    sgp41.executeConditioning(DEFAULT_RH, DEFAULT_T, srawVoc);
    conditioning_s--;
  } else {
    // Conditioning done. Detach this task and start logging
    sgp41Conditioner.detach();
  }
}

void setup() {
  Wire.begin();
  sgp41.begin(Wire);
  sgp41Conditioner.attach(1, conditionSgp41);
  sht4x.begin(Wire);
  rtc.begin();
  Serial.begin(115200);

  Serial.println("Insert SD card");
  delay(10000);

  while (!SD.begin(SD_SELECT_PIN, sd, 4000000, "/sd", 5, true)) {
    log_e("SD initialization failed");
  }

  WiFi.onEvent(onGotIP, WiFiEvent_t::SYSTEM_EVENT_STA_GOT_IP);
  WiFi.onEvent(onDisconnected, WiFiEvent_t::SYSTEM_EVENT_STA_DISCONNECTED);
  WiFi.begin(SSID, PASSWORD);

  sensorLogger.attach(LOG_INTERVAL, logSensors);
}

void loop() {}
