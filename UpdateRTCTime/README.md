This sketch fetches the current time from NTP servers and writes it to the RTC 
clock on board. For it the work, the device should during the bootup be in the
range of the access point with the SSID and password specified in the `Config.h`
file.
