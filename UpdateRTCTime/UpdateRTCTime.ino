#include <WiFi.h>
#include "RTClib.h"
#include "time.h"
#include "Config.h"

RTC_DS3231 rtc;

void printLocalTime() {
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

void onGotIP(WiFiEvent_t event, WiFiEventInfo_t info) {
  // Use UTC
  configTime(0, 0,
             "ntp1.rwth-aachen.de",
             "ntp2.rwth-aachen.de",
             "ntp3.rwth-aachen.de");
  printLocalTime();
  rtc.adjust(DateTime(time(NULL)));

  //disconnect WiFi as it's no longer needed
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
}

void setup() {
  Serial.begin(115200);
  rtc.begin();
  WiFi.onEvent(onGotIP, WiFiEvent_t::SYSTEM_EVENT_STA_GOT_IP);
  WiFi.begin(SSID, PASSWORD);
}

void loop() {
  delay(1000);
  Serial.println(rtc.now().timestamp());
}
